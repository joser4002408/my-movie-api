from fastapi import FastAPI, status
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel
from utils.jwt_manager import create_token

from middlewares.error_handler import ErrorHandler

# 
from config.database import engin, Base
from routers.movie import movie_router
from routers.user import user_router


app = FastAPI()

app.title = "Mi primer API con FastAPI"
app.version = "0.0.1"
#Esto es para capturar el error y poderlo mostrar
app.add_middleware(ErrorHandler)
app.include_router(user_router)
app.include_router(movie_router)


Base.metadata.create_all(bind=engin)

movies = [
    {
        "id": 1,
        "title": "The Godfather",
        "overview": "Spanning the years 1945 to 1955, a chronicle of the fictional Italian-American Corleone crime family. When organized crime family patriarch, Vito Corleone barely survives an attempt on his life, his youngest son, Michael steps in to take care of the would-be killers, launching a campaign of bloody revenge.",
        "year": 1972,
        "rating": 8.7,
        "category": "Acción"
    },
    {
        "id": 2,
        "title": "Avatar",
        "overview": "Spanning the years 1945 to 1955, a chronicle of the fictional Italian-American Corleone crime family. When organized crime family patriarch, Vito Corleone barely survives an attempt on his life, his youngest son, Michael steps in to take care of the would-be killers, launching a campaign of bloody revenge.",
        "year": 2000,
        "rating": 8.7,
        "category": "Acción"
    },
    {
        "id": 3,
        "title": "Caraotas",
        "overview": "Spanning the years 1945 to 1955, a chronicle of the fictional Italian-American Corleone crime family. When organized crime family patriarch, Vito Corleone barely survives an attempt on his life, his youngest son, Michael steps in to take care of the would-be killers, launching a campaign of bloody revenge.",
        "year": 2000,
        "rating": 8.7,
        "category": "Acción"
    }
]

# PRIMER ENDPOINT
@app.get("/", tags=["home"])
def message():
    return HTMLResponse("<h1>¡Hola Mundo!</h1>")



