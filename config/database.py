import os
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

sql_file_name = "../database.sqlite"
# LO QUE HACEMOS EN LA LINEA SIGUIENTE ES LEER EL DIRECTORIO ACTUAL
base_dir = os.path.dirname(os.path.realpath(__file__))


# URL DE LA BASE DE DATOS
database_url = f"sqlite:///{os.path.join(base_dir, sql_file_name)}"

# ENGINE REPRESENTA EL MOTOR DE LA BASE DE DATOS
engin = create_engine(database_url, echo=True)

# CREAMOS LA SESION
Session = sessionmaker(bind=engin)

Base = declarative_base()