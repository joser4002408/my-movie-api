from fastapi import FastAPI, Body
from fastapi.responses import HTMLResponse
from pydantic import BaseModel
from typing import Optional

app = FastAPI()

app.title = "Mi primer API con FastAPI"
app.version = "0.0.1"

class Movie(BaseModel):
    # id: int | None = None
    id: Optional[int] = None
    title: str
    overview: str
    year: int
    rating: float
    category: str

movies = [
    {
        "id": 1,
        "title": "The Godfather",
        "overview": "Spanning the years 1945 to 1955, a chronicle of the fictional Italian-American Corleone crime family. When organized crime family patriarch, Vito Corleone barely survives an attempt on his life, his youngest son, Michael steps in to take care of the would-be killers, launching a campaign of bloody revenge.",
        "year": 1972,
        "rating": 8.7,
        "category": "Acción"
    },
    {
        "id": 2,
        "title": "Avatar",
        "overview": "Spanning the years 1945 to 1955, a chronicle of the fictional Italian-American Corleone crime family. When organized crime family patriarch, Vito Corleone barely survives an attempt on his life, his youngest son, Michael steps in to take care of the would-be killers, launching a campaign of bloody revenge.",
        "year": 2000,
        "rating": 8.7,
        "category": "Acción"
    },
    {
        "id": 3,
        "title": "Caraotas",
        "overview": "Spanning the years 1945 to 1955, a chronicle of the fictional Italian-American Corleone crime family. When organized crime family patriarch, Vito Corleone barely survives an attempt on his life, his youngest son, Michael steps in to take care of the would-be killers, launching a campaign of bloody revenge.",
        "year": 2000,
        "rating": 8.7,
        "category": "Acción"
    }
]

# PRIMER ENDPOINT
@app.get("/", tags=["home"])
def message():
    return HTMLResponse("<h1>¡Hola Mundo!</h1>")



@app.get("/movies", tags=["movies"])
def get_movies():
    return movies

@app.get("/movies/{id}", tags=["movies"])
def get_movie(id: int):
    # MANERA DEL PROFESOR
    # for item in movies:
    #     if item["id"] == id:
    #         movie = item
    #         return movie
        
    # return []

    movie = next((movie for movie in movies if movie["id"] == id), [])

    return movie

@app.get('/movies/', tags=["movies"])
# ESTE ES UN QUERY PARAM
def get_movies_by_category(category: str, year: int):
    # return category
    # movie = next((movie for movie in movies if movie["category"] == category and movie["year"] == year), [])
    movie = []

    for item in movies:
        if item["category"] == category and item["year"] == year:
            movie.append(item)

    peliculas = [ movie for movie in movies if movie["category"] == category and movie["year"] == year]
    return peliculas

@app.post("/movies", tags=["movies"])
# def create_movie(id: int = Body(), title: str = Body(), overview: str = Body(), year: int = Body(), rating: float = Body(), category: str = Body()):
def create_movie(movie: Movie):
    movies.append({
        "id": id,
        "title": title,
        "overview": overview,
        "year": year,
        "rating": rating,
        "category": category
    })
    # movies.append(movie)
    return movies

# MODIFICACION
@app.put("/movies/{id}", tags=["movies"])
def update_movie(id: int, title: str = Body(), overview: str = Body(), year: int = Body(), rating: float = Body(), category: str = Body()):
    movie = next((movie for movie in movies if movie["id"] == id), [])
    if movie:
        movie["title"] = title
        movie["overview"] = overview
        movie["year"] = year
        movie["rating"] = rating
        movie["category"] = category

        return movie
    else:
        return "No se encontró la película"
    
# ELIMINACION
@app.delete("/movies/{id}", tags=["movies"])
def delete_movie(id: int):
    movie = next((movie for movie in movies if movie["id"] == id), [])
    if movie:
        movies.remove(movie)
        return movies
    else:
        return "No se encontró la película"