from fastapi import APIRouter

from fastapi import Path, Query, status, Depends
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List

from fastapi.encoders import jsonable_encoder

# 
from config.database import Session
from models.movie import Movie as MovieModel

from middlewares.jwt_bearer import JWTBearer

from services.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter()




@movie_router.get("/movies", tags=["movies"], response_model=list[Movie], status_code=200, dependencies=[Depends(JWTBearer())])
def get_movies() -> List[Movie]:
    db = Session()
    # result = db.query(MovieModel).all()
    result = MovieService(db).get_movies()
    
    return JSONResponse(content=jsonable_encoder(result), status_code=200)

@movie_router.get("/movies/{id}", tags=["movies"], response_model=Movie, status_code=200)
def get_movie(id: int = Path(ge=1, le=2000)) -> Movie:
    # MANERA DEL PROFESOR
    # for item in movies:
    #     if item["id"] == id:
    #         movie = item
    #         return movie
        
    # return []

    db = Session()
    # result = db.query(MovieModel).filter(MovieModel.id == id).first()
    result =  MovieService(db).get_movie(id)

    if not result:
        return JSONResponse(
            content={
                "error": "No se encontró la película"
            }, status_code=status.HTTP_400_BAD_REQUEST)
        
    return JSONResponse(content=jsonable_encoder(result), status_code=200)

    # movie = next((movie for movie in movies if movie["id"] == id), [])

    # if not movie:
    #     return JSONResponse(content={
    #         "error": "No se encontró la película"
    #     }, status_code=status.HTTP_400_BAD_REQUEST)

    # return JSONResponse(content=movie, status_code=status.HTTP_200_OK)

@movie_router.get('/movies/', tags=["movies"], response_model=list[Movie], status_code=200)
# ESTE ES UN QUERY PARAM
def get_movies_by_category(category: str = Query(min_length=5, max_length=15)) -> List[Movie]:
    # return category
    # movie = next((movie for movie in movies if movie["category"] == category and movie["year"] == year), [])
    movie = []

    db = Session()
    # result = db.query(MovieModel).filter(MovieModel.category == category, MovieModel.year == year).all()
    result = MovieService(db).get_movies_by_category(category)

    # for item in movies:
    #     if item["category"] == category and item["year"] == year:
    #         movie.append(item)

    # peliculas = [ movie for movie in movies if movie["category"] == category and movie["year"] == year]
    if not result:
        return JSONResponse(
            content={
                "error": "No se encontró la película"
            }, status_code=status.HTTP_400_BAD_REQUEST)
    
    return JSONResponse(content=jsonable_encoder(result), status_code=status.HTTP_200_OK)

@movie_router.post("/movies", tags=["movies"], response_model=dict, status_code=201)
# def create_movie(id: int = Body(), title: str = Body(), overview: str = Body(), year: int = Body(), rating: float = Body(), category: str = Body()):
def create_movie(movie: Movie) -> dict:
    # VERIFICAMOS SI LA PELICULA YA EXISTE
    # if any(m["id"] == movie.id for m in movies):
    #     return JSONResponse(content={
    #         "error": "Ya existe una película con ese ID"
    #     }, status_code=status.HTTP_400_BAD_REQUEST)
    
    db = Session()
    # new_movie = MovieModel(**movie.dict())

    # db.add(new_movie)
    # db.commit()
    # movie_dict = movie.model_dump()
    MovieService(db).create_movie(movie)

    # movies.append(movie_dict)
    # movies.append(movie)
    # return movies
    return JSONResponse(content={
        "message": "Pelicula creada exitosamente",
        # "movie": movie
    }, status_code=status.HTTP_201_CREATED)

# MODIFICACION
@movie_router.put("/movies/{id}", tags=["movies"], response_model=dict, status_code=200)
def update_movie(id: int, movie: Movie) -> dict:

    db = Session()
    # db.query(MovieModel).filter(MovieModel.id == id).update(movie.dict())
    # result = db.query(MovieModel).filter(MovieModel.id == id).first()
    result = MovieService(db).get_movies(id)
    if not result:
        return JSONResponse(
            content={
                "error": "No se encontró la película"
            }, status_code=status.HTTP_400_BAD_REQUEST)
    
    MovieService(db).update_movie(id, movie)
    # result.title = movie.title
    # result.overview = movie.overview
    # result.year = movie.year
    # result.rating = movie.rating
    # result.category = movie.category

    # db.commit()

    return JSONResponse(content={
        "message": "Pelicula actualizada exitosamente",
        # "movie": movie
    }, status_code=status.HTTP_200_OK)

    # movie = next((movie for movie in movies if movie["id"] == id), [])
    # if movie:
    #     movie["title"] = movie.title
    #     movie["overview"] = movie.overview
    #     movie["year"] = movie.year
    #     movie["rating"] = movie.rating
    #     movie["category"] = movie.category

    #     # return movie
    #     return JSONResponse(content={
    #         "message": "Pelicula actualizada exitosamente",
    #         # "movie": movie
    #     }, status_code=status.HTTP_200_OK)
    # else:
    #     # return "No se encontró la película"
    #     return JSONResponse(content={
    #         "error": "No se encontró la película"
    #     }, status_code=status.HTTP_400_BAD_REQUEST)
    
# ELIMINACION
@movie_router.delete("/movies/{id}", tags=["movies"], response_model=dict, status_code=200)
def delete_movie(id: int) -> dict:
    db = Session()
    # result = db.query(MovieModel).filter(MovieModel.id == id).first()
    result = MovieService(db).get_movies(id)
    if not result:
        return JSONResponse(
            content={
                "error": "No se encontró la película"
            }, status_code=status.HTTP_400_BAD_REQUEST)
    # db.delete(result)
    # db.commit()
    MovieService(db).delete_movie(id)
    return JSONResponse(content={
        "message": "Pelicula eliminada exitosamente",
        # "movie": movie
    }, status_code=status.HTTP_200_OK)

    # movie = next((movie for movie in movies if movie["id"] == id), [])
    # if movie:
    #     movies.remove(movie)
    #     # return movies
    #     return JSONResponse(content={
    #         "message": "Pelicula eliminada exitosamente",
    #         # "movie": movie
    #     }, status_code=status.HTTP_200_OK)
    # else:
    #     # return "No se encontró la película"
    #     return JSONResponse(content={
    #         "error": "No se encontró la película"
    #     }, status_code=status.HTTP_400_BAD_REQUEST)