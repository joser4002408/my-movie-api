
from fastapi import  status
from fastapi.responses import JSONResponse
from fastapi import APIRouter
from pydantic import BaseModel
from utils.jwt_manager import create_token
from schemas.user import User

user_router = APIRouter()

@user_router.post("/login", tags=["auth"], response_model=dict, status_code=200)
def login(user: User) -> dict:
    if user.email == "admin@gmail.com" and user.password == "admin":
        token: str = create_token(user.dict())
        return JSONResponse(content={
            "message": "Inicio de sesión exitoso",
            "token": token
        }, status_code=status.HTTP_200_OK)
    
    return user.dict()